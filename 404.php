<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->

	<title>404 | NeoCut HTML5 Template</title>

	<meta name="description" content="">
	<meta name="author" content="">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<!--[if lte IE 8]>
		<script type="text/javascript" src="http://explorercanvas.googlecode.com/svn/trunk/excanvas.js"></script>
	<![endif]-->

    <!-- **Favicon** -->
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link href="css/animations.css" rel="stylesheet" media="all" />
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/shortcode.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/isotope.css" type="text/css">
    <link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
	<link href="css/superfish.css" rel="stylesheet" media="all" />
    <link href="css/webfont.css" rel="stylesheet" type="text/css" />

    <!-- **Additional - stylesheets** -->
    <link href="css/pace-theme-loading-bar.css" rel="stylesheet" media="all" />

    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:500' rel='stylesheet' type='text/css'>

    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
    <![endif]-->
</head>

<body>
<?php include "header.php"; ?>
        <div id="main"><!-- Main  -->
        	<section class="fullwidth-background">
        		<div class="breadcrumb-wrapper">
                    <div class="container">
                       <div class="breadcrumb">
                       		<a href="index.php">Home</a>
                            <span class="fa fa-angle-right"> </span>
                            <h4>Missing Page</h4>
                            <h5 class="breadcrumb-title">Error</h5>
                       </div>
                    </div>
                </div>
            </section>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <section id="primary" class="content-full-width">
            	<div class="container">
                	<div class="error-404 aligncenter">
                    	<h1>Oops... Page Not Found!</h1>
                        <h2>404</h2>
                        <h3>Sorry the Page Could not be Found here.</h3>
                        <h4>Try using the button below to go to main page of the site</h4>
                        <a class="dt-sc-button small btn-icon effect" href="index.php">
                            <i class="fa fa-home"></i>
                            Go to Home
                        </a>
                    </div>
                </div>
                <div class="fullwidth-section news-letter">
            	<div class="hr-invisible-very-small"></div>
                <div class="hr-invisible-very-very-small"></div>
                    <div class="container">
                        <div class="column dt-sc-two-fifth first">
                            <h2>We are Ready to serve you!</h2>
                        </div>
                        <div class="column dt-sc-three-fifth">
                            <div class="alignleft">
                                <h2>Subscribe your <span>News letter</span></h2>
                                <p>you can unsubscribe any time you want</p>
                            </div>
                            <div class="alignright">
                                <form class="subscribe-frm" method="post" name="frmnewsletter" action="php/subscribe.php">
                                    <input type="email" placeholder="Enter email" required value="" name="mc_email" >
                                    <input class="dt-sc-button small" type="submit" value="Send" name="submit">
                                </form>
                                <div id="ajax_subscribe_msg"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		</div><!-- End of Main -->
        <div class="clear"></div>
        <div class="hr-invisible"></div>
        	<?php include "footer.php";?>
	</div><!-- End of Inner-Wrapper -->
</div><!-- End of Wrapper -->

<!-- **jQuery** -->
<script src="js/jquery.js"></script>
<script src="js/jsplugins.js" type="text/javascript"></script>
<script src="js/custom.js"></script>
</body>
</html>
