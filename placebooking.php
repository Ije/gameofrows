<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>

<body>
<?php include "header.php"; ?>
        <div id="main"><!-- Main  -->
        	<section class="fullwidth-background">
        		<div class="breadcrumb-wrapper">
                    <div class="container">
                       <div class="breadcrumb">
                       		<a href="index.php">Home</a>
                            <span class="fa fa-angle-right"> </span>
                            <h4>Make an Appointment</h4>
                            <h5 class="breadcrumb-title">Appointment</h5>
                       </div>
                    </div>
                </div>
            </section>
            <div class="hr-invisible"></div>
            <div class="clear"></div>
            <section id="primary" class="content-full-width">
            	<div class="container">
                    <div class="column dt-sc-one-third first">
                    	<div class="dt-sc-datepicker">
                            <div class="datepicker ll-skin-latoja"></div>
                            <div class="date-select">
                            	<h4 class="date-title">
                                	Selected day
                                </h4>
                                <div class="date">
                                   	01.
                                    <span>Apr 2015</span>
                                </div>
                                <img src="images/cal-separator.png" alt="" title="" class="cal-separator" />
                                <ul class="dt-sc-booking">
                                	<li class="booked">Booked</li>
                                    <li class="select">Selected Date</li>
                                    <li class="dt-sc-open">Open</li>
                                </ul>
                                <h5 class="dt-sc-cond">*Cond Depends Season</h5>
                            </div>
                        </div>
                        <div class="opening-time">
                            <div class="alignleft">
                              <!--  <img title="" alt="" src="http://placehold.it/78x160&text=Image"> -->
                            </div>
                            <div class="alignright">
                                <h3>Working Hours</h3>
                                <ul>
                                    <li>
                                       <span>Monday - Friday</span>
                                       <h4> 9am - 6pm </h4>
                                     </li>
                                     <li>
                                       <span>Saturday</span>
                                       <h4> 8am - 8pm </h4>
                                     </li>
                                     <li>
                                       <span>Sunday</span>
                                       <h4> 10am - 4pm</h4>
                                     </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="column dt-sc-two-third">
                    	<form class="" action="appointment.php" method="post" id ="contact-form" >
                            <div class="column dt-sc-one-half first">
                                <p><input type="text" name="username" placeholder="Your Name (required)" required/> </p>
                                <p><input type="text" name="phonenumber" placeholder="Your Phone (required)" required/></p>
																<p><input id="datepicker" type="text" placeholder="" name="datepicker" required><span class="icon-date-picker fa fa-calendar"></span></p>

                                <p><input type="email" name="email" placeholder="Email" required/></p>

                                </div>

                            </div>
                            <div class="column dt-sc-one-half">
                                <p><textarea class="message"  placeholder="Leave a message here" required rows="10" cols="5" name="txtmessage"></textarea></p>
                                <p><input type="submit" name="submit" value="Book Now" id="submitMe"></p>
                            </div>
                        </form>

                       <script>
                        $( function() {
                          $( "#datepicker" ).datepicker();
                        });

                          </script>
                          <!--
                        $("#submitMe").click(function() {
                          $.ajax({
                              type: 'POST',
                              url: "appointment.php",
                              dataType: "json",
                              data: $('#contact-form').serialize(),
                              success: function(data) {
                                  console.log("Done");

                              }
                          });
                        return false;
                        });
                      -->

      </div>
                </div>
                <div class="clear"></div>
                <div class="hr-invisible"></div>
                <div class="fullwidth-section news-letter">
            	<div class="hr-invisible-very-small"></div>
                <div class="hr-invisible-very-very-small"></div>
                    <div class="container">
                        <div class="column dt-sc-two-fifth first">
                            <h2>We are Ready to serve you!</h2>
                        </div>
                        <div class="column dt-sc-three-fifth">
                            <div class="alignleft">
                                <h2>Subscribe your <span>News letter</span></h2>
                                <p>you can unsubscribe any time you want</p>
                            </div>
                            <div class="alignright">
                                <form class="subscribe-frm" method="post" name="frmnewsletter" action="php/subscribe.php">
                                    <input type="email" placeholder="Enter email" required value="" name="mc_email" >
                                    <input class="dt-sc-button small" type="submit" value="Send" name="submit">
                                </form>
                                <div id="ajax_subscribe_msg"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- End of Main  -->
        <div class="clear"></div>
        <div class="hr-invisible"></div>
        	<?php include "footer.php";?>
    </div><!-- End of Inner-Wrapper -->
</div><!-- End of Wrapper -->

<!-- **jQuery** -->
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

<script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($) {
	$( ".datepicker" ).datepicker({
		inline: true,
		showOtherMonths: true
	});
});
</script>

<script src="js/jsplugins.js" type="text/javascript"></script>
<script src="js/custom.js"></script>

</body>
</html>
