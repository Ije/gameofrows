<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>

</head>

<body>
<?php include "header.php"; ?>
        <div id="main"><!-- Main  -->
        	<section class="fullwidth-background">
        		<div class="breadcrumb-wrapper">
                    <div class="container">
                       <div class="breadcrumb">
                       		<a href="index.html">Home</a>
                            <span class="fa fa-angle-right"> </span>
                            <h4>Portfolio Details With LHS</h4>
                            <h5 class="breadcrumb-title">Portfolio</h5>
                       </div>
                    </div>
                </div>
            </section>
            <div class="hr-invisible"></div>
            <div class="container"><!-- Container -->
            	<section id="primary" class="page-with-sidebar with-right-sidebar">
                	<div class="content">
                    	<div class="portfolio-single">
                            <ul class="portfolio-slider">
                                <li>
                                   <img src="http://placehold.it/1060x550&text=Portfolio+Image" alt="portfolio1" title="It’s more than a haircut!">
                                </li>
                                <li>
                                    <img src="http://placehold.it/1060x550&text=Portfolio+Image" alt="portfolio2" title="It’s more than a haircut!">
                                </li>
                                <li>
                                    <img src="http://placehold.it/1060x550&text=Portfolio+Image" alt="portfolio3" title="It’s more than a haircut!">
                                </li>
                                <li>
                                    <img src="http://placehold.it/1060x550&text=Portfolio+Image" alt="portfolio4" title="It’s more than a haircut!">
                                </li>
                                <li>
                                    <img src="http://placehold.it/1060x550&text=Portfolio+Image" alt="portfolio5" title="It’s more than a haircut!">
                                </li>
                            </ul>
                            <div class="bx-controls-direction">
                                <a class="bx-prev" href=""></a>
                                <a class="bx-next" href=""></a>
                            </div>
                        </div>
                        <div class="hr-invisible-medium"></div>
                        <h3 class="border-title"> IT'S MORE THAN A HAIRCUT! </h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem ve   l eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
                        <ul class="info portfolio-single">
                            <li>
                                <span>Customer :</span>
                                <h4>John Doe</h4>
                            </li>
                            <li>
                                <span>Live Demo :</span>
                                <h4><a href="#">www.sitename.com</a></h4>
                            </li>
                            <li>
                                <span>Date :</span>
                                <h4>10June, 2015</h4>
                            </li>
                            <li>
                                <span>Tag :</span>
                                <h4>mockup, mobile, <a>Hair Cut</a>, web, development, interfaces, design, graphic design, app, qaro</h4>
                            </li>
                        </ul>
                        <ul class="dt-sc-social-icons portfolio-single">
                        	<span>Link's :</span>
                            <li class="facebook"><a href="#" class="fa fa-facebook"></a></li>
                            <li class="twitter"><a href="#" class="fa fa-twitter"></a></li>
                            <li class="google-plus"><a href="#" class="fa fa-google-plus"></a></li>
                            <li class="tumblr"><a href="#" class="fa fa-tumblr"></a></li>
                            <li class="pinterest"><a href="#" class="fa fa-pinterest-p"></a></li>
                        </ul>
                        <!--<a class="dt-sc-button small effect1" target="new" href="#">Visit Website</a>
                        <div class="hr-invisible-very-small"></div>-->
                        <div class="post-pagination alignright">
                            <a class="dt-sc-button small effect prev" title="" href=""> </a>
                            <a class="dt-sc-button small effect back-to-work" title="" href="portfolio.html"> </a>
                            <a class="dt-sc-button small effect next" title="" href=""> </a>
                        </div>
                    </div>
                </section>
            	<section id="secondary" class="secondary-right-sidebar">
                	<aside class="widget searchwidget">
                    	<form class="searchform" name="searchform" action="#" method="post">
                            <p>
                            	<input type="text" required placeholder="Enter Keyword" name="username">
                                <input class="icon-search" type="submit" name="btnsubscribe" value="">
                            </p>
                        </form>
                    </aside>
                    <aside class="widget widget_categories">
                    	<h3 class="border-title">
                            Categories
                        </h3>
                        <ul>
                            <li>
                                <a href="#" title="">
                                	Cut & Style
                                	<span>7</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                	Advanced Styling
                                	<span>20</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                	Detan & Bleach
                                	<span>12</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                	Facials
                                	<span>17</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                	Manicure
                                	<span>9</span>
                                </a>
                            </li>
                        </ul>
                    </aside>
                    <div class="hr-invisible-very-small"></div>
                    <div class="hr-invisible-very-small"></div>
                    <div class="hr-invisible-very-very-small"></div>
                    <div class="clear"></div>
                    <aside class="widget tweetbox">
                    	<h3 class="border-title">
                            Tweet Updates
                        </h3>
                        <ul class="tweet_list">
                        	<li class="tweet">
                            	<span class="tweet-text">
                                	<a href="#">@ Ipsome </a>vehicula turpis sed lutpat Quisque vitae quam neque. Morbi cilisis placer at dapibus. <br><a href="#">http://t.co/9xy6KdchNc</a>
                                </span>
                                <br>
                                <span class="tweet-time">
                                	1 hours ago
                                </span>
                            </li>
                            <li class="tweet">
                            	<span class="tweet-text">
                                	<a href="#">@ Ipsome </a>vehicula turpis sed lutpat Quisque vitae quam neque. Morbi cilisis placer at dapibus. <br><a href="#">http://t.co/9xy6KdchNc</a>
                                </span>
                                <br>
                                <span class="tweet-time">
                                	3 hours ago
                                </span>
                            </li>
                            <li class="tweet">
                            	<span class="tweet-text">
                                	<a href="#">@ Ipsome </a>vehicula turpis sed lutpat Quisque vitae quam neque. Morbi cilisis placer at dapibus. <br><a href="#">http://t.co/9xy6KdchNc</a>
                                </span>
                                <br>
                                <span class="tweet-time">
                                	4 hours ago
                                </span>
                            </li>
                        </ul>
                    </aside>
                </section>
		  	</div><!-- End of Container -->
            <div class="hr-invisible-very-small"> </div>
            <div class="hr-separator"></div>
            <div class="hr-invisible"></div>
            <h2 class="border-title aligncenter animate" data-delay="100" data-animation="animated fadeInDown"> Similar Portfolio </h2>
            <div class="dt-sc-portfolio-container isotope no-space"> <!-- **dt-sc-portfolio-container Starts Here** -->
                <div class="portfolio dt-sc-one-half column no-space all-sort womens">
                    <figure>
                        <img src="http://placehold.it/1060x550&text=Portfolio+Image" alt="portfolio1" title="It’s more than a haircut!">
                        <figcaption>
                            <div class="fig-content">
                                <a class="zoom" href="http://placehold.it/1060x550&text=Portfolio+Image" data-gal="prettyPhoto[gallery]">
                                    <span class="hexagon">
                                        <span class="corner1"></span>
                                        <span class="corner2"></span>
                                        <span class="image-overlay-inside"></span>
                                    </span>
                                </a>
                                <a class="link" href="portfolio-detail-rhs.php">
                                    <span class="hexagon">
                                        <span class="corner1"></span>
                                        <span class="corner2"></span>
                                        <span class="image-overlay-inside"></span>
                                    </span>
                                </a>
                                <h5><a href="portfolio-detail.html">It’s more than a haircut!</a></h5>
                            </div>
                        </figcaption>
                    </figure>
                </div>
                <div class="portfolio dt-sc-one-half column no-space all-sort popular massage">
                    <figure>
                        <img src="http://placehold.it/1060x550&text=Portfolio+Image" alt="portfolio2" title="It’s more than a haircut!">
                        <figcaption>
                            <div class="fig-content">
                                <a class="zoom" href="http://placehold.it/1060x550&text=Portfolio+Image" data-gal="prettyPhoto[gallery]">
                                    <span class="hexagon">
                                        <span class="corner1"></span>
                                        <span class="corner2"></span>
                                        <span class="image-overlay-inside"></span>
                                    </span>
                                </a>
                                <a class="link" href="portfolio-detail-rhs.html">
                                    <span class="hexagon">
                                        <span class="corner1"></span>
                                        <span class="corner2"></span>
                                        <span class="image-overlay-inside"></span>
                                    </span>
                                </a>
                                <h5><a href="portfolio-detail.html">It’s more than a haircut!</a></h5>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
            <div class="fullwidth-background">
                <div class="container">
                    <ul class="dt-sc-social-network tooltip-container">
                        <li class="animate" data-delay="100" data-animation="animated fadeIn"><a href="#" class="fa fa-google-plus tooltip"><span class="tooltip-content">Google+</span></a></li>
                        <li class="animate" data-delay="300" data-animation="animated fadeIn"><a href="#" class="fa fa-facebook tooltip"><span class="tooltip-content">Facebook</span></a></li>
                        <li class="animate" data-delay="500" data-animation="animated fadeIn"><a href="#" class="fa fa-twitter tooltip"><span class="tooltip-content">Twitter</span></a></li>
                    </ul>
                </div>
            </div>
		</div><!-- End of Main -->
        <div class="clear"></div>
        <div class="hr-invisible"></div>
        	<?php include "footer.php";?>
	</div><!-- End of Inner-Wrapper -->
</div><!-- End of Wrapper -->

<!-- **jQuery** -->
<script src="js/jquery.js"></script>
<script src="js/jsplugins.js" type="text/javascript"></script>
<script src="js/custom.js"></script>
</body>
</html>
