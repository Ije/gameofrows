<?php

require_once "init.php";
if(isset($_POST['submit']))
{
  $name=$_POST['username'];
$message=$_POST['message'];
$email=$_POST['email'];


try
{

$conn = new PDO("mysql:host=$server;dbname=$dbname", $servername, $serverpassword);

$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$sql = "INSERT into comment (name,message,email,post_time) VALUES (:name,:message,:email,NOW())";
$statement = $conn->prepare($sql);
$statement->bindParam('name',$name,PDO::PARAM_STR);
$statement->bindParam('message',$message,PDO::PARAM_STR);
$statement->bindParam('email',$email,PDO::PARAM_STR);
//$statement->execute();
if($statement->execute()===true)
{

Header("Location:blog-detail-rhs.php");
}else{

echo "There was an error while saving your comment";
}



}catch(PDOException $e)
{
echo $e->getMessage();
}
}
?>
