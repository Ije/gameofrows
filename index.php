<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
	<?php include "header.php"; ?>


            <!-- **Slider Section** -->

            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div class="container">
                <div class="column dt-sc-one-third first animate" data-delay="100" data-animation="animated fadeIn">
                    <div class="dt-sc-ico-content type1">
                        <div class="icon">
                            <span class="hexagon">
                                <span class="corner1"></span>
                                <span class="corner2"></span>
                                <img src="images/icon-img4.png" alt="" title="" />
                                <span class="hexagon2"></span>
                            </span>
                        </div>
                        <h3>
                            <a target="_blank" href="#"> 100% satisfication </a>
                        </h3>
                    </div>
                </div>
                <div class="column dt-sc-one-third animate" data-delay="300" data-animation="animated fadeIn">
                    <div class="dt-sc-ico-content type1">
                        <div class="icon">
                            <span class="hexagon">
                                <span class="corner1"></span>
                                <span class="corner2"></span>
                                <img src="images/icon-img1.png" alt="" title="" />
                                <span class="hexagon2"></span>
                            </span>
                        </div>
                        <h3>
                            <a target="_blank" href="#"> QUALIFIED STAFFS </a>
                        </h3>
                    </div>
                </div>
                <div class="column dt-sc-one-third animate" data-delay="500" data-animation="animated fadeIn">
                    <div class="dt-sc-ico-content type1">
                        <div class="icon">
                            <span class="hexagon">
                                <span class="corner1"></span>
                                <span class="corner2"></span>
                                <img src="images/icon-img5.png" alt="" title="" />
                                <span class="hexagon2"></span>
                            </span>
                        </div>
                        <h3>
                            <a target="_blank" href="#"> Natural Things </a>
                        </h3>
                    </div>
                </div>
             </div>
            <div class="claer"></div>
            <div class="hr-invisible"></div>
            <div class="fullwidth-section dt-sc-parallax-section dt-sc-counters">
            	<div class="fullwidth-bg">
                    <div class="container">
                    <div class="hr-invisible"></div>
                        <h2 class="border-title aligncenter animate" data-delay="100" data-animation="animated fadeInDown">
                            Our Numbers
                        </h2>
												<center>
                        <div class="hr-invisible-very-very-small"></div>

                        <div class="column dt-sc-one-fourth animate" data-delay="300" data-animation="animated fadeIn">
                            <div class="dt-sc-counter">
                            	<div class="counter-effect"></div>
                                <div class="dt-sc-counter-number">
                                	<p data-value="65">65</p>
                                </div>
                                <h5> hair Styles </h5>
                            </div>
                        </div>
                        <div class="column dt-sc-one-fourth animate" data-delay="500" data-animation="animated fadeIn">
                            <div class="dt-sc-counter">
                            	<div class="counter-effect"></div>
                                <div class="dt-sc-counter-number">
                                	<p data-value="451">451</p>
                                </div>
                                <h5> Satisfied Customers </h5>
                            </div>
                        </div>
                        <div class="column dt-sc-one-fourth animate" data-delay="700" data-animation="animated fadeIn">
                            <div class="dt-sc-counter">
                            	<div class="counter-effect"></div>
                                <div class="dt-sc-counter-number">
                                	<p data-value="100">100</p><span>+</span>
                                </div>
                                <h5> Beauty Tips </h5>
                            </div>
                        </div>
                        <div class="hr-invisible"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div class="fullwidth-section dt-service-hme">
            	<div class="container">
                    <h2 class="border-title aligncenter animate" data-delay="100" data-animation="animated fadeInDown">
                        Our Services
                    </h2>
                    <div class="column dt-sc-one-fourth first animate" data-delay="100" data-animation="animated fadeIn">
                        <div class="dt-sc-service type1">
                            <h3><a href="#">Cutting</a></h3>
                            <figure class="gallery-thumb">
                                <img class="attachment-gallery-with-shape first-img" title="" alt="" src="http://placehold.it/130x130">
                                <img class="attachment-gallery-with-shape second-img" title="" alt="" src="http://placehold.it/130x130">
                            </figure>
                            <div class="gallery-details">
                                <p>Ius ferri velit sanctus cu, sed at soleat accusata. Dictas prompta et  Ut placerat legendos interpret.</p>
                            </div>
                        </div>
                    </div>
                    <div class="column dt-sc-one-fourth animate" data-delay="300" data-animation="animated fadeIn">
                        <div class="dt-sc-service type1">
                            <h3><a href="#">STYLING</a></h3>
                            <figure class="gallery-thumb">
                                <img class="attachment-gallery-with-shape first-img" title="" alt="" src="http://placehold.it/130x130">
                                <img class="attachment-gallery-with-shape second-img" title="" alt="" src="http://placehold.it/130x130">
                            </figure>
                            <div class="gallery-details">
                                <p>Ius ferri velit sanctus cu, sed at soleat accusata. Dictas prompta et  Ut placerat legendos interpret.</p>
                            </div>
                        </div>
                    </div>
                    <div class="column dt-sc-one-fourth animate" data-delay="500" data-animation="animated fadeIn">
                        <div class="dt-sc-service type1">
                            <h3><a href="#">COLOURING</a></h3>
                            <figure class="gallery-thumb">
                                <img class="attachment-gallery-with-shape first-img" title="" alt="" src="http://placehold.it/130x130">
                                <img class="attachment-gallery-with-shape second-img" title="" alt="" src="http://placehold.it/130x130">
                            </figure>
                            <div class="gallery-details">
                                <p>Ius ferri velit sanctus cu, sed at soleat accusata. Dictas prompta et  Ut placerat legendos interpret.</p>
                            </div>
                        </div>
                    </div>
                    <div class="column dt-sc-one-fourth animate" data-delay="700" data-animation="animated fadeIn">
                        <div class="dt-sc-service type1">
                            <h3><a href="#">FACIALS</a></h3>
                            <figure class="gallery-thumb">
                                <img class="attachment-gallery-with-shape first-img" title="" alt="" src="http://placehold.it/130x130">
                                <img class="attachment-gallery-with-shape second-img" title="" alt="" src="http://placehold.it/130x130">
                            </figure>
                            <div class="gallery-details">
                                <p>Ius ferri velit sanctus cu, sed at soleat accusata. Dictas prompta et  Ut placerat legendos interpret.</p>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
        <!--end of services section   -->

            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div class="container">
            	<h2 class="border-title aligncenter animate" data-delay="100" data-animation="animated fadeInDown">
                    Our Blog
                </h2>
                <div class="hr-invisible-very-very-small"></div>
                <div class="column dt-sc-one-third first">
                	<article class="blog-entry animate" data-delay="100" data-animation="animated fadeIn">
                        <div class="entry-thumb">
                            <a href="blog-detail-rhs.html"><img src="http://placehold.it/1060x717&text=Blog+Image+1" alt="" title="" /></a>
                        </div>
                        <div class="entry-details">
                            <div class="entry-title">
                                <h4><a href="blog-detail-rhs.html"> Hair Styling </a></h4>
                            </div>
                            <div class="entry-meta">
                                <div class="date">
                                    29 <span>MAY</span>
                                </div>
                            </div>
                            <div class="entry-metadata">
                                <p>Think of a news blog that's filled with content hourly on the day of going live. </p>
                                <div class="hr-separator"></div>
                                <div class="author"><i class="fa fa-user"></i><a href="#">admin</a></div>
                                <div class="tags"><i class="fa fa-tag"></i><a href="#">Hair,</a><a href="#"> Eye</a></div>
                                <div class="comments"><a href="#"><i class="fa fa-comment"></i>3</a></div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="column dt-sc-one-third">
                	<article class="blog-entry animate" data-delay="300" data-animation="animated fadeIn">
                        <div class="entry-thumb">
                            <a href="blog-detail-rhs.html"><img src="http://placehold.it/1060x717&text=Blog+Image+2" alt="" title="" /></a>
                        </div>
                        <div class="entry-details">
                            <div class="entry-title">
                                <h4><a href="blog-detail-rhs.html"> Manicure & Hand Spa </a></h4>
                            </div>
                            <div class="entry-meta">
                                <div class="date">
                                    30 <span>MAY</span>
                                </div>
                            </div>
                            <div class="entry-metadata">
                                <p>Think of a news blog that's filled with content hourly on the day of going live. </p>
                                <div class="hr-separator"></div>
                                <div class="author"><i class="fa fa-user"></i><a href="#">admin</a></div>
                                <div class="tags"><i class="fa fa-tag"></i><a href="#">Leg,</a><a href="#"> Nail</a></div>
                                <div class="comments"><a href="#"><i class="fa fa-comment"></i>3</a></div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="column dt-sc-one-third">
                	<article class="blog-entry animate" data-delay="500" data-animation="animated fadeIn">
                        <div class="entry-thumb">
                            <a href="blog-detail-rhs.html"><img src="http://placehold.it/1060x717&text=Blog+Image+3" alt="" title="" /></a>
                        </div>
                        <div class="entry-details">
                            <div class="entry-title">
                                <h4><a href="blog-detail-rhs.html"> Hair Curling </a></h4>
                            </div>
                            <div class="entry-meta">
                                <div class="date">
                                    01 <span>JUN</span>
                                </div>
                            </div>
                            <div class="entry-metadata">
                                <p>Think of a news blog that's filled with content hourly on the day of going live. </p>
                                <div class="hr-separator"></div>
                                <div class="author"><i class="fa fa-user"></i><a href="#">admin</a></div>
                                <div class="tags"><i class="fa fa-tag"></i><a href="#">Hair</a></div>
                                <div class="comments"><a href="#"><i class="fa fa-comment"></i>3</a></div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="hr-invisible"></div>
			</div>
            <div class="hr-invisible"></div>
            <div class="fullwidth-section dt-sc-center">
            	<h2 class="border-title aligncenter align animate" data-delay="100" data-animation="animated fadeInDown">
                    Make a special Appointment
                </h2>
            	<div class="container">
                    <form class="" method="post"  action="appointment.php">
                        <div class="column dt-sc-one-half first animate" data-delay="100" data-animation="animated fadeIn">
                            <p><input type="text" name="username" placeholder="Your Name (required)" required/> </p>
                            <p><input type="email" name="email" placeholder="Email (required)" required/></p>
                        </div>
                        <div class="column dt-sc-one-half animate" data-delay="300" data-animation="animated fadeIn">
                            <p><input type="text" required name="phonenumber" placeholder="Your Phone (required)"/></p>
                            <p><input id="datepicker" type="text" placeholder="Date of Appointment" value="" name="datepicker" required><span class="icon-date-picker fa fa-calendar"></span></p>
                        </div>
                        <div class="column dt-sc-one-column animate" data-delay="500" data-animation="animated fadeIn">
                            <p><textarea class="message" required rows="10" placeholder="Comments(required)" cols="5" name="txtmessage"></textarea></p>
                        </div>
                        <ul class="phone">
                            <li><i class="fa fa-mobile-phone"></i>
                            (03) 1234 5678</li>
                            <li><i class="fa fa-envelope-o"></i>
                            <a href="mailto:yourname@somemail.com">neocut@gmail.com</a></li>
                        </ul>
                        <div class="form-row aligncenter">
                            <input type="submit"  name="submit">
                        </div>
                    </form>
										<script>
										 $( function() {
											 $( "#datepicker" ).datepicker(); //this calls the jquery function that displays the popup
																													//calendar while the user fills the form.
										 });

											 </script>
                    <!--<div id="ajax_contactform_msg"> </div> -->
                </div>
                <div class="dt-sc-ico-content type1 select">
                    <div class="icon">
                        <span class="hexagon">
                            <span class="corner1"></span>
                            <span class="corner2"></span>
                            <img src="images/icon-img5.png" alt="" title="" />
                            <span class="hexagon1">
                                <span class="corner1"></span>
                                <span class="corner2"></span>
                            </span>
                        </span>
                    </div>
                 </div>
                 <div class="news-letter center">
                    <div class="subscribe-form">
                        <div class="alignleft">
                            <h2>Subscribe your <span>News letter</span></h2>
                            <p>you can unsubscribe any time you want</p>
                        </div>
                        <div class="alignright">
                            <form class="subscribe-frm" method="post" name="frmnewsletter" action="php/subscribe.php">
                                <input type="email" placeholder="Enter email" required value="" name="mc_email" >
                                <input class="dt-sc-button small" type="submit" value="Send" name="submit">
                            </form>
                            <div id="ajax_subscribe_msg"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="hr-invisible"></div>
            <div class="hr-invisible-small"></div>

						<?php include "footer.php";?>
            <!-- End of Main -->
    </div><!-- Inner-Wrapper -->
</div><!-- Wrapper -->

<!-- **jQuery** -->
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-migrate.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/layerslider.js"></script>

<!-- **Slider** -->

<!--Responsible for the calendar popup -->
<script type="text/javascript">
$(document).ready(function() {
	$("#datepicker").datepicker();
});
</script>

<script type="text/javascript" src="js/jsplugins.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>
