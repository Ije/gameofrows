<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>

</head>

<body>
	<?php include "header.php";?>
<!--place a form here -->


            <div class="clear"></div>
            <section id="primary" class="content-full-width">
                <div class="fullwidth-section">
                    <div id="contact_map2"> </div>
                </div>
                <div class="hr-invisible-medium"></div>
                <div class="container">
                	<h2 class="border-title aligncenter animate" data-delay="100" data-animation="animated fadeInDown"> WE ARE HERE </h2>
                    <div class="column dt-sc-one-third first">
                    	<h3 class="title">
                            Head Office
                        </h3>
                        <div class="dt-sc-contacts-info">
                            <div class="icon">
                                <i class="fa fa-rocket"></i>
                            </div>
                            <p>
                                123 Building, Street name,<br>City Name , Code-6542.
                            </p>
                            <div class="icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <p>
                                (00) 123 456 7890
                            </p>
                            <div class="icon">
                                <i class="fa fa-globe"></i>
                            </div>
                            <p>
                                <a href="http://www.google.com" target="new">www.google.com</a>
                            </p>
                            <div class="icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <p>
                                <a href="mailto:lms@gmail.com">support@ownmail.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="column dt-sc-one-third">
                    	<h3 class="title">
                            Gobi Branch
                        </h3>
                        <div class="dt-sc-contacts-info">
                            <div class="icon">
                                <i class="fa fa-rocket"></i>
                            </div>
                            <p>
                                123 Building, Street name,<br>City Name , Code-6542.
                            </p>
                            <div class="icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <p>
                                (00) 123 456 7890
                            </p>
                            <div class="icon">
                                <i class="fa fa-globe"></i>
                            </div>
                            <p>
                                <a href="http://www.google.com" target="new">www.google.com</a>
                            </p>
                            <div class="icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <p>
                                <a href="mailto:lms@gmail.com">support@ownmail.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="column dt-sc-one-third">
                    	<h3 class="title">
                            CBE Branch
                        </h3>
                        <div class="dt-sc-contacts-info">
                            <div class="icon">
                                <i class="fa fa-rocket"></i>
                            </div>
                            <p>
                                123 Building, Street name,<br>City Name , Code-6542.
                            </p>
                            <div class="icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <p>
                                (00) 123 456 7890
                            </p>
                            <div class="icon">
                                <i class="fa fa-globe"></i>
                            </div>
                            <p>
                                <a href="http://www.google.com" target="new">www.google.com</a>
                            </p>
                            <div class="icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <p>
                            	<a href="mailto:lms@gmail.com">support@ownmail.com</a>
                            </p>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="hr-invisible-medium"></div>
                    <h2 class="border-title aligncenter animate" data-delay="100" data-animation="animated fadeInDown"> CONTACT US </h2>
                    <p class="aligncenter">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam<br> malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                    <div class="hr-invisible-very-small"></div>
                    <form class="enquiry-form animate" data-delay="100" data-animation="animated fadeInDown" name="enqform" action="php/send.php" method="post">
                    	<div class="column dt-sc-one-half first">
                            <p>
                                <label>Your Name (*Required)</label>
                                <input type="text" required placeholder="" title="Please enter your name" name="txtname">
                            </p>
                        </div>
                        <div class="column dt-sc-one-half">
                            <p>
                                <label>Your Mail Address (*Required)</label>
                                <input type="email" placeholder="" name="txtemail">
                            </p>
                        </div>
                        <p class="column dt-sc-one-column">
                            <label>Type Your Message (*Required)</label>
                            <textarea class="message" name="txtmessage" required cols="5" rows="10"></textarea>
                        </p>
                        <div class="aligncenter">
	                        <input type="submit" name="submit" value="send now">
                        </div>
                    </form>
                    <div id="ajax_contactform_msg"> </div>
                </div>
                <div class="hr-invisible"></div>
                <div class="fullwidth-background">
                	<div class="container">
                        <ul class="dt-sc-social-network tooltip-container">
                            <li class="animate" data-delay="100" data-animation="animated fadeIn"><a href="#" class="fa fa-google-plus tooltip"><span class="tooltip-content">Google+</span></a></li>
                            <li class="animate" data-delay="300" data-animation="animated fadeIn"><a href="#" class="fa fa-facebook tooltip"><span class="tooltip-content">Facebook</span></a></li>
                            <li class="animate" data-delay="500" data-animation="animated fadeIn"><a href="#" class="fa fa-twitter tooltip"><span class="tooltip-content">Twitter</span></a></li>
                        </ul>
                    </div>
                </div>
            </section>
        </div><!-- End of Main Section -->
        <div class="clear"></div>
        <div class="hr-invisible"></div>
      	<?php include "footer.php";?>
	</div><!-- End of Inner-Wrapper -->
</div><!-- End of Wrapper -->

<!-- **jQuery** -->
<script src="js/jquery.js"></script>
<script src="js/jsplugins.js" type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>
