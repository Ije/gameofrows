<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->

	<title>Home | Game of FROS </title>

	<meta name="description" content="">
	<meta name="author" content="">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<!--[if lte IE 8]>
		<script type="text/javascript" src="http://explorercanvas.googlecode.com/svn/trunk/excanvas.js"></script>
	<![endif]-->

    <!-- **Favicon** -->
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <!-- **CSS - stylesheets** -->
    <link id="default-css" href="style.css" rel="stylesheet" media="all" />
    <link href="css/animations.css" rel="stylesheet" media="all" />
    <link id="skin-css" href="skins/green/style.css" rel="stylesheet" media="all" />
    <link href="css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/shortcode.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/isotope.css" type="text/css">
    <link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <link href="css/superfish.css" rel="stylesheet" media="all" />
    <link href="css/webfont.css" rel="stylesheet" type="text/css" />

    <!-- **Additional - stylesheets** -->
    <link href="css/pace-theme-loading-bar.css" rel="stylesheet" media="all" />
    <link id="layer-slider" rel="stylesheet"  href="css/layerslider.css" media="all" />

    <!-- **Font Awesome** -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- **Google - Fonts** -->
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Shadows+Into+Light' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>

    <!--[if IE 7]>
    <link rel="stylesheet" href="css/font-awesome-ie7.min.css" />
    <![endif]-->

    <!-- jQuery -->
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<!--
<div id="loader-wrapper">
    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_four"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_one"></div>
            </div>
        </div>
    </div>
</div> -->
<div class="wrapper"><!-- Wrapper -->
    <div class="inner-wrapper"><!-- Inner-Wrapper -->
    	<header id="header" class="type1"><!-- Header -->
        	<div class="container">
                 <div class="main-menu-container">
                	<div class="main-menu">
                        <div id="logo">
                          <!--LOGO SIDE -->  <a title="NeoCut" href="index.php"><img title="NeoCut" alt="NeoCut" src="images/logo.png"></a>
                        </div>
                        <div class="dt-sc-header-shape"><a class="dt-sc-button medium alignright" href="placebooking.php">Make an Appointment</a><span class="hexagon"> <span class="corner1"></span><span class="corner2"></span></span></div>
                        <div id="primary-menu">
                          <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
                            <nav id="main-menu">
                            	<ul class="menu">
                                    <li class="current_page_item dropdown menu-item-simple-parent"><a href="index.php">Home</a>

                                        <a class="dt-menu-expand">+</a>
                                    </li>
                                    <!--<li class=""><a href="services.html">Services</a></li> -->
                                    <li class="dropdown menu-item-simple-parent"><a href="adminlogin.php">Admin Login</a>

                                         <a class="dt-menu-expand">+</a>
                                    </li>
                                    <li class="dropdown menu-item-simple-parent"><a href="blog-with-rhs.php">Blog</a>

                                         <a class="dt-menu-expand">+</a>
                                    </li>

                                    <li class="dropdown menu-item-simple-parent"><a href="contact.php">Contact</a>
																			<li class="dropdown menu-item-simple-parent"><a href="faq.php">FAQs</a>

                                         <a class="dt-menu-expand">+</a>
                                    </li>
                               </ul>
                            </nav>
               			</div>
              		</div>
             	 </div>
        	</div>
        </header><!-- End of Header -->
