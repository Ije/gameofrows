
<footer id="footer">
  <div class="footer-widgets-wrapper">
      <div class="container">
          <div class="column dt-sc-one-fourth first">
              <aside class="widget widget_text">
                  <h3 class="widgettitle">Contact Us</h3>
                    <div class="textwidget">
                      <div class="dt-sc-contact-info">
                          <div class="icon">
                              <i class="fa fa-envelope"></i>
                            </div>
                            <p><a href="mailto:lms@gmail.com">admin@ownmail.com</a></p>
                        </div>
                        <div class="dt-sc-contact-info">
                          <div class="icon">
                              <i class="fa fa-phone"></i>
                            </div>
                            <p>(01) 123 456 7890</p>
                        </div>
                        <div class="dt-sc-contact-info address">
                          <p>1235, Building name,<br>Busy Commercial Street, Big City, <br>Ny - 4215</p>
                        </div>
                    </div>
                </aside>
            </div>
            <div class="column dt-sc-one-fourth">
              <aside class="widget widget_text">
                  <h3 class="widgettitle">Our Best Service</h3>
                    <div class="textwidget">
                      <ul>
                          <li><a href="#">Hair Cutting</a></li>
                            <li><a href="#">Hair Styling</a></li>
                            <li><a href="#">Detan & Bleach</a></li>
                            <li><a href="#">Facials</a></li>
                            <li><a href="#">Hair Colouring</a></li>
                        </ul>
                    </div>
                </aside>
           </div>
           <div class="column dt-sc-one-fourth">
              <aside class="widget widget_text">
                  <h3 class="widgettitle">Products We Use</h3>
                    <div class="textwidget">
                      <ul>
                          <li><a href="#">Shampoo</a></li>
                            <li><a href="#">Conditioner</a></li>
                            <li><a href="#">Treatment</a></li>
                            <li><a href="#">Styling Products</a></li>
                            <li><a href="#">Brushes & Combs</a></li>
                        </ul>
                    </div>
                </aside>
           </div>
           <div class="column dt-sc-one-fourth">
              <aside class="widget widget_text">
                  <h3 class="widgettitle">Additional Info</h3>
                    <div class="textwidget">
                      <ul>
                          <li><a href="#">Brands</a></li>
                            <li><a href="#">Special Vouchers</a></li>
                            <li><a href="#">Affiliates</a></li>
                            <li><a href="#">Specialist in Shop</a></li>
                            <li><a href="#">Special Services</a></li>
                        </ul>
                        <a href="#" class="app-link alignleft"><img src="images/appstore-google.png" alt="" title="" /></a>
                        <a href="#" class="app-link alignright"><img src="images/appstore.png" alt="" title="" /></a>
                    </div>
                </aside>
           </div>
        </div>
    </div>
    <div class="container">
        <div class="hr-invisible"></div>
        <ul class="footer-links">
            <li>Top Category:</li>
            <li><a href="#">Service</a></li>
            <li><a href="#">Products</a></li>
            <li><a href="#">Specialists</a></li>
            <li><a href="#">Offers</a></li>
        </ul>
        <div class="hr-invisible-very-small"></div>
        <div class="hr-invisible-very-very-small"></div>
        <div class="copyright-content">
          <p>
                © 2015 Game of FROS. Developed by
                <a title="" href="http://twitter.com/VanessaOsuka">Nessa</a>
            </p>
        </div>
        <div class="hr-invisible-small"></div>
    </div>
</footer>
</div>
